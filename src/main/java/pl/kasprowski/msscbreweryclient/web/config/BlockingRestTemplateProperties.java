package pl.kasprowski.msscbreweryclient.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("rest.template.blocking")
public class BlockingRestTemplateProperties {

    private int totalConnections;
    private int perRouteConnections;
    private int connectionRequestTimeout;
    private int socketTimeout;

    public int getTotalConnections() {
        return totalConnections;
    }

    public void setTotalConnections(final int totalConnections) {
        this.totalConnections = totalConnections;
    }

    public int getPerRouteConnections() {
        return perRouteConnections;
    }

    public void setPerRouteConnections(final int perRouteConnections) {
        this.perRouteConnections = perRouteConnections;
    }

    public int getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(final int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(final int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }
}
