package pl.kasprowski.msscbreweryclient.web.client;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.kasprowski.msscbreweryclient.web.model.BeerDto;

import java.net.URI;
import java.util.UUID;

import static java.lang.String.format;

@Component
@ConfigurationProperties(value = "sfg.brewery.beer", ignoreUnknownFields = false)
public class BreweryClient {

    private String apiHost;
    private String endpoint;

    private final RestTemplate restTemplate;

    public BreweryClient(final RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void setApiHost(final String apiHost) {
        this.apiHost = apiHost;
    }

    public void setEndpoint(final String endpoint) {
        this.endpoint = endpoint;
    }

    public BeerDto getBeerById(final UUID id) {
        return restTemplate.getForObject(format("%s%s%s", apiHost, endpoint, id.toString()), BeerDto.class);
    }

    public URI saveBeer(final BeerDto dto) {
        return restTemplate.postForLocation(format("%s%s", apiHost, endpoint), dto);
    }

    public void updateBeer(final BeerDto dto) {
        restTemplate.put(format("%s%s%s", apiHost, endpoint, dto.getId().toString()), dto);
    }

    public void deleteBeer(final UUID id) {
        restTemplate.delete(format("%s%s%s", apiHost, endpoint, id));
    }
}
