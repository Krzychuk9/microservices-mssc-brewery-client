package pl.kasprowski.msscbreweryclient.web.client;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.kasprowski.msscbreweryclient.web.model.CustomerDto;

import java.net.URI;
import java.util.UUID;

import static java.lang.String.format;

@Component
@ConfigurationProperties(value = "sfg.brewery.customer", ignoreUnknownFields = false)
public class CustomerClient {
    private String apiHost;
    private String endpoint;

    private RestTemplate restTemplate;

    public CustomerClient(final RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void setApiHost(final String apiHost) {
        this.apiHost = apiHost;
    }

    public void setEndpoint(final String endpoint) {
        this.endpoint = endpoint;
    }

    public CustomerDto getById(final UUID id) {
        return restTemplate.getForObject(format("%s%s%s", apiHost, endpoint, id), CustomerDto.class);
    }

    public URI saveCustomer(final CustomerDto dto) {
        return restTemplate.postForLocation(format("%s%s", apiHost, endpoint), dto);
    }

    public void updateCustomer(final CustomerDto dto) {
        restTemplate.put(format("%s%s%s", apiHost, endpoint, dto.getId()), dto);
    }

    public void deleteCustomer(final UUID id) {
        restTemplate.delete(format("%s%s%s", apiHost, endpoint, id));
    }
}
