package pl.kasprowski.msscbreweryclient.web.client;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.kasprowski.msscbreweryclient.web.model.BeerDto;

import java.net.URI;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BreweryClientTest {

    @Autowired
    private BreweryClient client;

    @Test
    void getBeerById() {
        final BeerDto beer = client.getBeerById(UUID.randomUUID());

        assertThat(beer).isNotNull();
    }

    @Test
    void saveBeer() {
        final BeerDto dto = BeerDto.builder()
                .beerName("name")
                .beerStyle("style")
                .upc(1L)
                .build();

        final URI location = client.saveBeer(dto);

        assertThat(location).isNotNull();
    }

    @Test
    void updateBeer() {
        final BeerDto dto = BeerDto.builder()
                .id(UUID.randomUUID())
                .beerName("name")
                .beerStyle("style")
                .upc(1L)
                .build();

        client.updateBeer(dto);
    }

    @Test
    void deleteBeer() {
        client.deleteBeer(UUID.randomUUID());
    }
}