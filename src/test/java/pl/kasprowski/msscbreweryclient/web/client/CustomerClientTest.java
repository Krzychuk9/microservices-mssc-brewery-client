package pl.kasprowski.msscbreweryclient.web.client;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.kasprowski.msscbreweryclient.web.model.CustomerDto;

import java.net.URI;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CustomerClientTest {

    @Autowired
    private CustomerClient client;

    @Test
    void getById() {
        final CustomerDto customer = client.getById(UUID.randomUUID());

        assertThat(customer).isNotNull();
    }

    @Test
    void saveCustomer() {
        final CustomerDto dto = CustomerDto.builder()
                .name("name")
                .build();

        final URI location = client.saveCustomer(dto);

        assertThat(location).isNotNull();
    }

    @Test
    void updateCustomer() {
        final CustomerDto dto = CustomerDto.builder()
                .id(UUID.randomUUID())
                .name("name")
                .build();

        client.updateCustomer(dto);
    }

    @Test
    void deleteCustomer() {
        client.deleteCustomer(UUID.randomUUID());
    }
}